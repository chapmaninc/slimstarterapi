<?php
define('ABSOLUTE_PATH', 'C:/Users/Andrew/projects/slim/experiment1/');
define('SOURCE_PATH', ABSOLUTE_PATH . 'src/');

use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use ChapmanDigital\Helpers\StringsHelper;
use ChapmanDigital\Helpers\EnvironmentHelper;
use ChapmanDigital\Helpers\EmailHelper;
use ChapmanDigital\Services\GroupService;
use ChapmanDigital\Services\UserService;
use ChapmanDigital\Services\TokenService;

// DIC configuration
$container = $app->getContainer();

//Override the default Not Found Handler
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        //Format of exception to return
        $data = [
            'message' => '404 Not Found'
        ];

        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($data));
    };
};

//Override the default Not Found Handler
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        //Format of exception to return
        $data = [
            'message' => 'Method must be one of: ' . implode(', ', $methods)
        ];

        return $c['response']
            ->withStatus(405)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($data));
    };
};

// Override the default PHP runtime error handler
$container['phpErrorHandler'] = function ($c) {
    return function ($request, $response, $error) use ($c) {
        //Format of exception to return
        $data = [
            'message' => 'Sorry, something went wrong: ' . $error
        ];

        return $c['response']
            ->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($data));
    };
};

// Generic error handler
$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        //Format of exception to return
        $data = [
            'message' => 'Sorry, something went wrong: ' . $exception->getMessage()
        ];

        return $c['response']
            ->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($data));
    };
};

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Service factory for the ORM
$container['db'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);

    $capsule->setEventDispatcher(new Dispatcher(new Container));

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates', [
        'cache' => '../cache'
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

$container['ErrorCodeManager'] = function ($c) use ($errorCodes) {
    return new ChapmanDigital\Libs\ErrorCodeManager($errorCodes);
};

$container['Authenticator'] = function ($container) use ($errorCodes) {
    $errorCodeManager = new ChapmanDigital\Libs\ErrorCodeManager($errorCodes);
    $table = $container->get('db')->table('token');
    return new \ChapmanDigital\Middleware\Authenticator($container, $errorCodeManager, $table);
};

$container['GeographyController'] = function ($c) {
    //$view = $c->get("view"); // retrieve the 'view' from the container
    $table = $c->get('db')->table('timezone');
    return new ChapmanDigital\Controllers\GeographyController($c, $table);
};

$container['GroupController'] = function ($c) {
    $table = $c->get('db')->table('group');
    return new \ChapmanDigital\Controllers\GroupController($c, $table);
};

$container['UserController'] = function ($c) {
    $table = $c->get('db')->table('user');
    return new \ChapmanDigital\Controllers\UserController($c, $table);
};

$container['StringsHelper'] = function ($c) {
    $helper = new StringsHelper();
    return $helper;
};

$container['EnvironmentHelper'] = function ($c) {
    $helper = new EnvironmentHelper($c);
    return $helper;
};

$container['EmailHelper'] = function ($c) {
    $helper = new EmailHelper($c);
    return $helper;
};

$container['GroupService'] = function ($container) {
    $groupService = new GroupService($container);
    return $groupService;
};

$container['UserService'] = function ($container) {
    $userService = new UserService($container);
    return $userService;
};

$container['TokenService'] = function ($container) {
    $tokenService = new TokenService($container);
    return $tokenService;
};
