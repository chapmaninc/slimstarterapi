<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' => false,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'slimstarter',
            'username' => 'xxx',
            'password' => 'yyy',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
        'email' => [
            'transportMethod' => 'SMTP',   // "SMTP", "SEND MAIL", OR "PHP MAIL"
            'serverHost' => 'localhost',
            'smtpServerPort' => 25,
            'smtpServerEncryptionMethod' => '',
            'smtpServerUsername' => '',
            'smtpServerPassword' => '',
            'sendmailCommand' => '/usr/sbin/sendmail -bs',
            'from' => 'me@mydomain.com',
            'fromName' => 'Jane Doe',
            'replyTo' => 'me@mydomain.com',
            'templateDir' => '%INSTALL_PATH%/templates/email/',
        ],
        'environment' => [
            'baseURL' => 'http://localhost/index.php',
            'installPath' => '/path/to/slimstarter',
            'dummyIP' => '216.58.204.35',
        ],
        'sessions' => [
            'tokenSecondsUntilTimeout' => 1800,
            'checkIPAddress' => true,
            'checkUserAgent' => true,
            'extendTokenAutomatically' => true,
        ],
        'user' => [
            'defaultRoleId' => 1,
            'adminRoleId' => 1
        ]
    ],
];
