<?php
namespace ChapmanDigital\Controllers;

use ChapmanDigital\Exceptions\UrlInvalidException;
use ChapmanDigital\Services\UserService;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Database\Query\Builder;
use Respect\Validation\Exceptions\NestedValidationException;
use ChapmanDigital\Exceptions\ItemDoesNotExistException;

/**
 * Class UserController
 * @package ChapmanDigital\Controllers
 */
class UserController extends BaseController
{
    protected $table;

    public function __construct(\Slim\Container $container, Builder $table)
    {
        parent::__construct($container);

        $this->table = $table;
    }

    /**
     * Handles a user login request.
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    public function login(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Read post vars into group array
        $email = $request->getParsedBodyParam('email');
        $password = $request->getParsedBodyParam('password');

        // Get user IP address and userAgent
        $ipAddress = $request->getAttribute('ip_address');
        $userAgent = $request->getHeader('user_agent')[0] ?? '';

        try {
            $userService = new UserService($this->container);
            list($group, $user, $token) = $userService->login($email, $password, $ipAddress, $userAgent);
            return $response->withJson(['token' => $token->tokenCode, 'user' => $user, 'group' => $group]);
        } catch (NestedValidationException $exception) {
            return $this->handleError($response, ERRORCODE_VALIDATION_ERROR, $exception->getFullMessage());
        } catch (\Exception $exception) {
            return $this->handleError($response, ERRORCODE_LOGIN_FAILURE);
        }
    }

    /**
     * Adds a new user to the database for a specific group.
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    public function addUser(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Grab the logged in user from the request.  This is
        // done by the Authenticator middleware.
        $loggedInUser = $request->getAttribute('loggedInUser');

        // Read post vars into array
        $userData = [];

        $inputFields = [
            'firstName',
            'middleName',
            'lastName',
            'countryId',
            'timezoneId',
            'email',
            'password',
            'roleIds'
        ];

        foreach ($inputFields as $field) {
            $userData[$field] = $request->getParsedBodyParam($field);
        }

        // Attempt to add the new user to the database
        $userService = new UserService($this->container);

        try {
            $user = $userService->addNew($loggedInUser, $userData);
            return $response->withJson(['user' => $user]);
        } catch (NestedValidationException $exception) {
            return $this->handleError($response, ERRORCODE_VALIDATION_ERROR, $exception->getFullMessage());
        } catch (\Exception $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getMessage());
        }
    }

    /**
     * Deletes the specified user from the database
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param array $params Must contain 'userId' parameter
     * @return mixed
     * @throws UrlInvalidException
     */
    public function deleteUser(ServerRequestInterface $request, ResponseInterface $response, array $params)
    {
        $deleteId = intval($params['userId'] ?? 0);

        if (empty($deleteId)) {
            throw new UrlInvalidException();
        }

        // Grab the logged in user from the request.  This is
        // done by the Authenticator middleware.
        $loggedInUser = $request->getAttribute('loggedInUser');

        // Attempt to add the new user to the database
        $userService = new UserService($this->container);

        try {
            $userService->delete($loggedInUser, $deleteId, $request->getAttribute('ownRecordsOnly'));
        } catch (ItemDoesNotExistException $exception) {
            return $this->handleError($response, ERRORCODE_ITEM_DOES_NOT_EXIST, $exception->getMessage());
        } catch (NestedValidationException $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getFullMessage());
        } catch (\Exception $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getMessage());
        }
    }

    public function updateUser(ServerRequestInterface $request, ResponseInterface $response, array $args)
    {
        $updateId = intval($args['userId'] ?? 0);

        if (empty($updateId)) {
            throw new UrlInvalidException();
        }

        // Read post vars into array
        $userData = [];

        $inputFields = [
            'firstName',
            'middleName',
            'lastName',
            'countryId',
            'timezoneId',
            'email',
            'password',
            'roleIds'
        ];

        foreach ($inputFields as $field) {
            $userData[$field] = $request->getParsedBodyParam($field);
        }

        // Grab the logged in user from the request.
        $loggedInUser = $request->getAttribute('loggedInUser');

        // Attempt to add the new user to the database
        $userService = new UserService($this->container);

        try {
            $userService->update(
                $request,
                $loggedInUser,
                $updateId,
                $userData
            );
        } catch (ItemDoesNotExistException $exception) {
            return $this->handleError($response, ERRORCODE_ITEM_DOES_NOT_EXIST, $exception->getMessage());
        } catch (NestedValidationException $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getFullMessage());
        } catch (\Exception $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getMessage());
        }
    }
}
