<?php
namespace ChapmanDigital\Controllers;

use ChapmanDigital\Exceptions\EmailAlreadyExistsException;
use ChapmanDigital\Exceptions\ItemDoesNotExistException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Database\Query\Builder;
use Respect\Validation\Exceptions\NestedValidationException;

use ChapmanDigital\Models\Group;
use ChapmanDigital\Models\User;
use ChapmanDigital\Models\UserRole;
use ChapmanDigital\Services\GroupService;
use ChapmanDigital\Services\UserService;

/**
 * Class GeographyController
 * @package ChapmanDigital\Controllers
 */
class GroupController extends BaseController
{
    protected $table;

    public function __construct(\Slim\Container $container, Builder $table)
    {
        parent::__construct($container);

        $this->table = $table;
    }

    /**
     * Handles a new group and user registration request.
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    public function register(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Read post vars into group array
        $groupData = [];

        $inputFields = [
            'name',
            'firstName',
            'middleName',
            'lastName',
            'countryId',
            'timezoneId',
            'email',
            'password',
        ];

        foreach ($inputFields as $field) {
            $groupData[$field] = $request->getParsedBodyParam($field);
        }

        try {
            $adminRoleId = 1;

            /**
             * @var GroupService $groupService
             */
            $groupService = $this->container->get('GroupService');

            /**
             * @var UserService $userService
             */
            $userService = $this->container->get('UserService');

            /**
             * @var User $user
             * @var Group $group
             * @var UserRole $userRole
             */
            list($user, $group, $userRole) = $groupService->addNew($userService, $groupData);

            // Return user details minus any fields we want to suppress
            unset($user->password);

            return $response->withJson(['user' => $user, 'group' => $group]);
        } catch (NestedValidationException $exception) {
            return $this->handleError($response, $exception->getCode(), $exception->getMessage());
        } catch (EmailAlreadyExistsException $exception) {
            return $this->handleError($response, $exception->getCode(), $exception->getMessage());
        }
    }

    public function updateGroup(ServerRequestInterface $request, ResponseInterface $response, array $args)
    {
        $groupId = $args['groupId'] ?? 0;

        if (empty($groupId)) {
            throw new \Exception('Invalid group Id');
        }

        $inputDataArray = [];

        $inputFields = [
            'name',
            'enabled',
            'countryId',
            'timezoneId'
        ];

        foreach ($inputFields as $field) {
            $inputDataArray[$field] = $request->getParsedBodyParam($field);
        }

        $loggedInUser = $request->getAttribute('loggedInUser');

        $groupService = new GroupService($this->container);

        try {
            $groupService->update(
                $request,
                $loggedInUser,
                $groupId,
                $inputDataArray
            );
        } catch (ItemDoesNotExistException $exception) {
            return $this->handleError($response, ERRORCODE_ITEM_DOES_NOT_EXIST, $exception->getMessage());
        } catch (NestedValidationException $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getFullMessage());
        } catch (\Exception $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getMessage());
        }
    }
}