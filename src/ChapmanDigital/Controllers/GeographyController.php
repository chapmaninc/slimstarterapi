<?php
namespace ChapmanDigital\Controllers;

use ChapmanDigital\Helpers\EnvironmentHelper;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Database\Query\Builder;
use ChapmanDigital\Models\GeoModel;

/**
 * Class GeographyController
 * @package ChapmanDigital\Controllers
 */
class GeographyController extends BaseController
{
    protected $table;

    public function __construct(\Slim\Container $container, Builder $table)
    {
        parent::__construct($container);

        $this->table = $table;
    }

    /**
     * Loads all of the timezones, optionally for a specific country.
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param string $countryCode optional country code
     * @return mixed
     */
    public function getTimezones(ServerRequestInterface $request, ResponseInterface $response, $countryCode = '')
    {
        if (empty($countryCode)) {
            $timezones = $this->table->get();
        } else {
            $timezones = $this->table->where('countryCode', '=', $countryCode)->get();
        }

        return $response->withJson($timezones);
    }

    /**
     * Returns information about the users country, based on their IP address.
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    public function getUserCountry(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Get the user's IP Address
        $ipAddress = $request->getAttribute('ip_address');

        // Figure out which country the user belongs to based on the IP
        $geoModel = new GeoModel();

        /**
         * @var EnvironmentHelper $environmentHelper
         */
        $environmentHelper = $this->container->get('EnvironmentHelper');

        try {
            $data = $geoModel->getCountryFromIp(
                $ipAddress,
                $environmentHelper->getInstallPath() . '/cache/',
                $environmentHelper->getDummyIP()
            );

            return $response->withJson($data);
        } catch (\Exception $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getMessage());
        }
    }

    /**
     * Returns the timezones for the users country, determined automatically
     * by the users IP address.
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    public function getUserTimezones(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Get the user's IP Address
        $ipAddress = $request->getAttribute('ip_address');

        // Figure out which country the user belongs to based on their IP address
        $geoModel = new \ChapmanDigital\Models\GeoModel();

        /**
         * @var EnvironmentHelper $environmentHelper
         */
        $environmentHelper = $this->container->get('EnvironmentHelper');

        try {
            $data = $geoModel->getCountryFromIp(
                $ipAddress,
                $environmentHelper->getInstallPath() . '/cache/',
                $environmentHelper->getDummyIP()
            );

            // Ensure geo response looks valid and has appropriate content.
            if ((is_object($data)) && (property_exists($data, 'countryCode'))) {
                // Return the timezones that are particular to the users country
                return $this->getTimezones($request, $response, $data->countryCode);
            } else {
                return $this->handleError($response, 1);
            }
        } catch (\Exception $exception) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $exception->getMessage());
        }
    }
}