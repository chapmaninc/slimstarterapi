<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 06/03/2017
 * Time: 22:15
 */

namespace ChapmanDigital\Middleware;

use ChapmanDigital\Exceptions\UrlInvalidException;
use ChapmanDigital\Models\PermissionType;
use ChapmanDigital\Models\RolePermission;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ChapmanDigital\Exceptions\TokenExpiredException;
use ChapmanDigital\Exceptions\TokenInvalidException;
use ChapmanDigital\Exceptions\SlugInvalidException;
use ChapmanDigital\Exceptions\AccessDeniedException;
use ChapmanDigital\Helpers\EnvironmentHelper;
use ChapmanDigital\Services\TokenService;


class Authenticator
{
    private $container;
    private $errorCodeManager;
    private $table;

    public function __construct($container, $errorCodeManager, $table)
    {
        $this->container = $container;
        $this->errorCodeManager = $errorCodeManager;
        $this->table = $table;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        try {
            // An authenticated request MUST provide a valid access token.
            // The token can be passed either in the HTTP Headers e.g
            // Access-Token: abc
            // or in the HTTP Post parameters

            // First try and grab the access token from the headers
            $tokenCode = $request->getHeader('Access-Token')[0] ?? '';

            // If no joy, check the POST parameters
            if (empty($tokenCode)) {
                $tokenCode = $request->getParsedBodyParam('token');

                if (empty($tokenCode)) {
                    throw new AccessDeniedException();
                }
            }

            // Get the user's IP Address & User Agent
            $ipAddress = $request->getAttribute('ip_address');
            $userAgent = $request->getHeader('user_agent')[0] ?? '';

            // Get the logged in user using the tokenCode
            $tokenService = new TokenService($this->container);
            $loggedInUser = $tokenService->getUserFromToken($tokenCode, $ipAddress, $userAgent);

            $request = $request->withAttribute('loggedInUser', $loggedInUser);

            // Load the roles assigned to the user and set the roles into the request
            $userRoles = $loggedInUser->userRoles;

            $userRoleArray = [];

            foreach ($userRoles as $role) {
                $userRoleArray[] = $role;
            }

            

            $request = $request->withAttribute('userRoles', $userRoleArray);

            // Get the slug from the current URL
            $envHelper = new EnvironmentHelper($this->container);
            $slug = $envHelper->getUriSlug($request);

            // Load the permission type for this slug - there should only be 1
            /**
             * @var PermissionType $permissionType
             */
            $permissionTypes = PermissionType::where('slug', $slug)->get();
            if (count($permissionTypes) != 1) {
                throw new SlugInvalidException($slug);
            }

            $permissionType = $permissionTypes[0];

            /**
             * Load the permission records available to this user, based on their roles
             * and the permissionType that matches the request URL
             */
            $rolePermissions = RolePermission::getRolePermissionsForUserRolesAndPermissionType(
                $userRoleArray,
                $permissionType->permissionTypeId
            );

            // If the user has no permissions for this URL - throw a permission denied exception.
            if (($rolePermissions == null) || (count($rolePermissions) == 0)) {
                throw new AccessDeniedException();
            }

            /**
             * Determine if the user has permission to do this operation (create/read/update/delete)
             * based on their role permissions.  Note that the user may have permission to
             * access their own data only ($hasPermission == 1) or ALL group records ($hasPermission == 2).
             */
            $operationType = $this->getOperationTypeFromRequestMethod($request);
            $permissionLevel = RolePermission::hasPermission($rolePermissions, $operationType);

            if ($permissionLevel === RolePermission::PERMISSION_NO_PERMISSION) {
                throw new AccessDeniedException();
            }

            // Write the permissionLevel into the request
            $request = $request->withAttribute('permissionLevel', $permissionLevel);

            // If the user is reading, updating or deleting an existing record,
            // it's possible they might be trying to do so on a record that they did not
            // create.  See if this user has permission is restricted to their own records or
            // not.

            // Default to allowing the user to access their own records only.
            $ownRecordsOnly = true;

            if ($operationType != RolePermission::OPERATION_TYPE_CREATE) {
                foreach ($rolePermissions as $permission) {
                    if ($permission->ownRecordsOnly === 0) {
                        $ownRecordsOnly = false;
                        break;
                    }
                }
            }

            $request = $request->withAttribute('ownRecordsOnly', $ownRecordsOnly);
        } catch (\Exception $exception) {
            return $this->handleError($response, $exception->getCode(), $exception->getMessage());
        }

        $response = $next($request, $response);

        return $response;
    }

    /**
     * Converts a HTTP request method, e.g. "POST" into a permissionType int value, e.g. "1"
     * @param ServerRequestInterface $request
     * @return int
     */
    private function getOperationTypeFromRequestMethod(ServerRequestInterface $request) : int
    {
        switch ($request->getMethod()) {
            case "GET":
                return RolePermission::OPERATION_TYPE_READ;
                break;

            case "POST":
                return RolePermission::OPERATION_TYPE_CREATE;
                break;

            case "PUT":
                return RolePermission::OPERATION_TYPE_UPDATE;
                break;

            case "DELETE":
                return RolePermission::OPERATION_TYPE_DELETE;
                break;
        }

        return 0;
    }

    /**
     * Returns a standard error response in JSON encoding.
     * Note that error codes are stored in /errorCodes.php
     *
     * @param ResponseInterface $response
     * @param integer $errorCode
     * @param array $info
     * @return mixed
     */
    protected function handleError(ResponseInterface $response, $errorCode, $info = [])
    {
        // Get the error response and http status code
        list($errorData, $httpStatusCode) = $this->errorCodeManager->getErrorCodeResponse($errorCode);

        // If additional information was passed in the information array,
        // include that in the response.
        if (count($info) > 0) {
            $errorData['information'] = $info;
        }

        return $response->withJson($errorData, $httpStatusCode);
    }
}