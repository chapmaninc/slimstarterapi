<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 18/03/2017
 * Time: 13:52
 */

namespace ChapmanDigital\Services;

use ChapmanDigital\Exceptions\AccessDeniedException;
use ChapmanDigital\Exceptions\EmailAlreadyExistsException;
use ChapmanDigital\Models\Group;
use ChapmanDigital\Models\User;
use Respect\Validation\Validator as v;
use ChapmanDigital\Models\UserRole;
use Illuminate\Database\Capsule\Manager as DB;
use ChapmanDigital\Helpers\EmailHelper;
use Illuminate\Database\Eloquent\Model;
use ChapmanDigital\Exceptions\ItemDoesNotExistException;
use Psr\Http\Message\ServerRequestInterface;

class GroupService extends AbstractModelService
{
    public function addNew(UserService $userService, array $attributeArray) : array
    {
        // Convert group data to object
        $group = (object)$attributeArray;

        // Validate all inputs are sane
        $groupValidator =
            v::attribute('name', v::alnum()->length(1, 255))
                ->attribute('firstName', v::alnum()->noWhitespace()->length(1, 80))
                ->attribute('middleName', v::optional(v::alnum()->noWhitespace()->length(1, 80)))
                ->attribute('lastName', v::alnum()->noWhitespace()->length(1, 80))
                ->attribute('countryId', v::intVal())
                ->attribute('timezoneId', v::intVal())
                ->attribute('email', v::email())
                ->attribute('password', v::stringType()->length(6, 80));

        $groupValidator->assert($group);

        // Make sure a user with this email doesn't exist already.
        if ($userService->userWithEmailExists($group->email)) {
            throw new EmailAlreadyExistsException();
        }

        $group = new Group($attributeArray);
        $user = new User($attributeArray);
        $userRole = new UserRole();

        $defaultRoleId = $this->container->get('settings')['user']['defaultRoleId'] ?? 1;

        // Create group, user and userRole records in a transaction.
        DB::transaction(function () use ($userService, $attributeArray, $group, $user, $userRole, $defaultRoleId) {
            $group->enabled = 1;
            $group->save();

            $user->groupId = $group->groupId;
            $user->enabled = 1;
            $user->password = $userService->hashPassword($attributeArray['password']);
            $user->save();

            $group->createdByUserId = $user->userId;
            $group->save();

            $user->createdByUserId = $user->userId;
            $user->save();

            $userRole->roleId = $defaultRoleId;
            $userRole->userId = $user->userId;
            $userRole->save();
        });

        /**
         * @var EmailHelper $emailHelper
         */
        $emailHelper = $this->container->get('EmailHelper');

        $emailHelper->sendHtmlEmail(
            $user->email,
            $user->firstName . ' ' . $user->lastName,
            'Thanks for registering',
            'registration',
            [
                'first_name' => $user->firstName
            ]
        );

        // Return new user, group and userRole details
        return [$user, $group, $userRole];
    }

    public function delete(Model $loggedInUser, int $deleteId, bool $ownRecordsOnly) : void
    {
        // @todo Implement method
    }

    /**
     * Validates passed attributes to make sure all the required fields are present
     * and correct for the record type.
     * @param array $attributeArray
     * @param bool $newRecordMode
     * @throws \Exception
     */
    public function validate(array $attributeArray, bool $newRecordMode) : void
    {
        // Validate all inputs are sane
        $myValidator =
            v::attribute('name', v::alnum()->length(1, 255))
                ->attribute('enabled', v::intVal());
        ;

        $myValidator->assert((object)$attributeArray);
    }

    /**
     * Loads a singular model record
     * @param int $groupId The groupId the item belongs to
     * @return Model
     * @throws ItemDoesNotExistException
     */
    protected function getById(int $groupId) : Model
    {
        $items = Group::where('groupId', $groupId)
            ->get();

        if (count($items) != 1) {
            throw new ItemDoesNotExistException();
        }

        return $items[0];
    }

    /**
     * Updates the group record for the logged in user but only if they are an administrator.
     * @param ServerRequestInterface $request
     * @param Model $loggedInUser
     * @param int $updateId
     * @param array $attributeArray
     * @throws AccessDeniedException
     * @throws ItemDoesNotExistException
     */
    public function update(
        ServerRequestInterface $request,
        Model $loggedInUser,
        int $updateId,
        array $attributeArray
    ) {
        // Only admins can update a group.
        $userService = new UserService($this->getContainer());

        if (!$userService->isAdmin($request)) {
            throw new AccessDeniedException();
        }

        // Get the model item that the user is trying to update
        $item = $this->getById($loggedInUser->groupId);
        if (!$item) {
            throw new ItemDoesNotExistException();
        }

        parent::doUpdate($loggedInUser, $item, $request->getAttribute('ownRecordsOnly'), $attributeArray);
    }
}
