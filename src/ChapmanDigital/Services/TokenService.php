<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 19/03/2017
 * Time: 09:12
 */

namespace ChapmanDigital\Services;

use ChapmanDigital\Exceptions\TokenExpiredException;
use ChapmanDigital\Exceptions\TokenInvalidException;
use ChapmanDigital\Exceptions\ItemDoesNotExistException;
use ChapmanDigital\Models\Token;
use ChapmanDigital\Models\User;
use ChapmanDigital\Helpers\StringsHelper;
use Slim\Collection;
use Respect\Validation\Validator as v;
use Illuminate\Database\Eloquent\Model;

class TokenService extends AbstractModelService
{
    /**
     * Adds a new token to the database for the logged in user and returns the token object
     * @param User $loggedInUser
     * @param string $ipAddress
     * @param string $userAgent
     * @return Token
     */
    public function addNew(User $loggedInUser, string $ipAddress, string $userAgent = '') : Token
    {
        $tokenData = [
            'userId' => $loggedInUser->userId,
            'ipAddress' => $ipAddress
        ];

        // Validate all inputs are sane
        $validator =
            v::attribute('userId', v::intVal())
                ->attribute('ipAddress', v::stringType()->noWhitespace()->length(3, 255))
        ;

        $validator->assert((object)$tokenData);

        /**
         * @var Collection $settings
         */
        $settings = $this->container->get('settings');

        /**
         * @var StringsHelper $stringsHelper
         */
        $stringsHelper = $this->container->get('StringsHelper');

        $token = new Token($tokenData);
        $token->tokenCode = $this->createUniqueToken($stringsHelper);

        if (!empty($userAgent)) {
            $token->userAgentHash = md5($userAgent);
        }

        // Use the expiry timeout value in the settings if defined.
        $this->setTokenExpiry($token, $settings);

        $token->save();

        return $token;
    }

    /**
     * Deletes all of the tokens belonging to a particular user
     * that have expired.
     * @param int $userId
     */
    public function deleteUserTokens(int $userId)
    {
        // Load the associated group record
        $tokens = Token::where('userId', $userId)
            ->where('expiryDate', '<', date('Y-m-d H:i:s'))
            ->get();

        /**
         * @var Token $token
         */
        foreach ($tokens as $token) {
            $token->delete();
        }
    }

    /**
     * Loads a user object from the database,
     * using the passed token code to validate that the session
     * is ok.
     * @param string $tokenCode
     * @param string $ipAddress
     * @param string $userAgent
     * @return User
     * @throws TokenInvalidException
     * @throws TokenExpiredException
     * @throws \Exception
     */
    public function getUserFromToken(string $tokenCode, string $ipAddress = '', string $userAgent = '') : User
    {
        // Tokens must be 32 characters long
        if (strlen($tokenCode) != 32) {
            throw new TokenInvalidException();
        }

        // Attempt to load the token
        $token = Token::where('tokenCode', $tokenCode)->get()[0];
        if (!$token) {
            throw new TokenInvalidException();
        }

        // Ensure token expiry is valid
        if (strtotime($token->expiryDate) < time()) {
            throw new TokenExpiredException();
        }

        /**
         * @var Collection $settings
         */
        $settings = $this->container->get('settings');
        $checkIPAddress = $settings['sessions']['checkIPAddress'] ?? false;
        $checkUserAgent = $settings['sessions']['checkUserAgent'] ?? false;
        $extendTokenAutomatically = $settings['sessions']['extendTokenAutomatically'] ?? false;

        // If IP Address checking is enabled, ensure the token is from the same IP
        if (($checkIPAddress) && ((empty($ipAddress)) || ($token->ipAddress != $ipAddress))) {
            throw new TokenInvalidException();
        }

        // If UserAgent checking is on, ensure the user agent matches the token useragent.
        if (($checkUserAgent) && ((empty($userAgent)) || ($token->userAgentHash != md5($userAgent)))) {
            throw new TokenInvalidException();
        }

        // Load the user associated with the token
        $user = User::where('userId', $token->userId)->get()[0];
        if (!$user) {
            throw new \Exception('Invalid token user');
        }

        if ($extendTokenAutomatically) {
            $this->setTokenExpiry($token, $settings);
            $token->save();
        }

        // Return the user.
        return $user;
    }

    /**
     * Sets a token's expiry date based on the token timeout in the settings.
     * @param Token $token
     * @param Collection $settings#
     */
    private function setTokenExpiry(Token $token, Collection $settings)
    {
        // Use the expiry timeout value in the settings if defined.
        $token->expiryDate = date(
            'Y-m-d H:i:s',
            time() + $settings['sessions']['tokenSecondsUntilTimeout'] ?? 600
        );
    }

    /**
     * Generates a random token and ensures that the
     * token is not currently in use in the tokens db.
     * @param StringsHelper $stringsHelper
     * @return string
     */
    private function createUniqueToken(StringsHelper $stringsHelper) : string
    {
        $tokenCodeExists = true;
        $tokenCode = '';

        while ($tokenCodeExists) {
            $tokenCode = $stringsHelper->createRandomString(32);

            $tokens = Token::where('tokenCode', $tokenCode)
                ->get();

            if (count($tokens) == 0) {
                $tokenCodeExists = false;
            }
        }

        return $tokenCode;
    }

    /**
     * Loads a singular model record
     * @param int $groupId The groupId the item belongs to
     * @param string $tokenCode The id of the item to load
     * @return Model
     * @throws ItemDoesNotExistException
     */
    protected function getById(int $groupId, string $tokenCode) : Model
    {
        $items = Token::where('groupId', $groupId)
            ->where('tokenCode', $tokenCode)
            ->get();

        if (count($items) != 1) {
            throw new ItemDoesNotExistException();
        }

        return $items[0];
    }
}
