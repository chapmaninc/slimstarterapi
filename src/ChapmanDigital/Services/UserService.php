<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 18/03/2017
 * Time: 09:59
 */

namespace ChapmanDigital\Services;

use ChapmanDigital\Models\Role;
use ChapmanDigital\Models\UserRole;
use ChapmanDigital\Exceptions\EmailAlreadyExistsException;
use ChapmanDigital\Models\Group;
use ChapmanDigital\Models\User;
use Psr\Http\Message\ServerRequestInterface;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;
use Illuminate\Database\Eloquent\Model;
use \Slim\Container;
use ChapmanDigital\Exceptions\ItemDoesNotExistException;
use Illuminate\Database\Capsule\Manager as DB;

class UserService extends AbstractModelService
{
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Adds a new user to the database
     * @param Model $loggedInUser The user who is adding the new user
     * @param array $attributeArray The attributes of the new user
     * @return Model An instance of the user model that represents the new user
     * @throws EmailAlreadyExistsException
     * @throws \Exception
     */
    public function addNew(Model $loggedInUser, array $attributeArray) : Model
    {
        // Validate all inputs are sane
        $this->validate($attributeArray, true);

        // Ensure roleIds are ok
        if (!$this->validateRoleIds($attributeArray['roleIds'] ?? '')) {
            throw new \Exception('One or more of the provided role ids are invalid');
        }

        // Ensure that a user with the new email address doesn't already exist
        if ($this->userWithEmailExists($attributeArray['email'])) {
            throw new EmailAlreadyExistsException();
        }

        // Setup and populate new user object
        $user = new User($attributeArray);
        $user->groupId = $loggedInUser->groupId;
        $user->createdByUserId = $loggedInUser->userId;
        $user->enabled = 1;

        // Hash user password.
        $user->password = $this->hashPassword($attributeArray['password']);

        // If either country or timezone hasn't been provided, default them
        // from the related user group.
        if ((empty($user->countryId)) || (empty($user->timezoneId))) {
            $group = Group::find($loggedInUser->groupId);

            // This should never happen, but just in case.
            if (!$group) {
                throw new \Exception('Invalid logged in user group');
            }

            if (empty($user->countryId)) {
                $user->countryId = $group->countryId;
            }

            if (empty($user->timezoneId)) {
                $user->timezoneId = $group->timezoneId;
            }
        }

        // Create user and userRole records in a transaction.
        DB::transaction(function () use ($user, $attributeArray) {
            // Save the user
            if (!$user->save()) {
                throw new \Exception('Failed to save user');
            }

            // Create the role records.
            $roleIdArray = explode(',', $attributeArray['roleIds']);
            foreach ($roleIdArray as $roleId) {
                $userRole = new UserRole();

                $userRole->roleId = $roleId;
                $userRole->userId = $user->userId;

                if (!$userRole->save()) {
                    throw new \Exception('Unable to save user role');
                }
            }
        });

        // Remove password - we don't want this returned to any consuming client.
        unset($user->password);

        return $user;
    }

    /**
     * Checks to see if a user with a specific email address already exists.
     * @param string $emailAddress
     * @return bool
     */
    public function userWithEmailExists(string $emailAddress) : bool
    {
        // Make sure a user with this email doesn't exist already.
        $user = User::where('email', $emailAddress)->get();
        return (count($user) > 0);
    }

    /**
     * Returns a password hashed using BCRYPT.
     * @param string $password
     * @return bool|string
     */
    public function hashPassword(string $password)
    {
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
    }

    /**
     * Logs the user into the system.
     * @param string $emailAddress
     * @param string $password
     * @param string $ipAddress
     * @param string $userAgent
     * @return array
     * @throws \Exception
     */
    public function login(string $emailAddress, string $password, string $ipAddress, string $userAgent = '') : array
    {
        // Convert group data to object
        $credentials = (object)['email' => $emailAddress, 'password' => $password];

        // Validate all inputs are sane
        $userValidator = v::attribute('email', v::email())
            ->attribute('password', v::stringType())
        ;

        $userValidator->assert($credentials);

        // Make sure a user with this email exists
        $users = User::where('email', $credentials->email)->get();
        if (count($users) != 1) {
            throw new \Exception('Login failure');
        }

        /**
         * @var User $user
         */
        $user = $users[0];

        // Ensure the password matches
        if (!password_verify($credentials->password, $user->password)) {
            throw new \Exception('Login failure');
        }

        // Load the associated group record
        $groups = Group::where('groupId', $user->groupId)->get();
        if (count($groups) != 1) {
            throw new \Exception('Invalid user group');
        }

        /**
         * @var Group $group
         */
        $group = $groups[0];

        /**
         * @var TokenService $tokenService
         */
        $tokenService = $this->container->get('TokenService');

        // Delete any tokens the user previously had that have expired.
        $tokenService->deleteUserTokens($user->userId);

        // Create a new token
        $token = $tokenService->addNew($user, $ipAddress, $userAgent);

        // Remove password - we don't want it passed to consuming clients
        unset($user->password);

        return [$group, $user, $token];
    }

    /**
     * Loads a singular model record
     * @param int $groupId The groupId the item belongs to
     * @param int $itemId The id of the item to load
     * @return Model
     * @throws ItemDoesNotExistException
     */
    protected function getById(int $groupId, int $itemId) : Model
    {
        $items = User::where('groupId', $groupId)
            ->where('userId', $itemId)
            ->get();

        if (count($items) != 1) {
            throw new ItemDoesNotExistException();
        }

        return $items[0];
    }

    /**
     * Ensures that all of the role ids contained within a
     * csv string are valid.
     * @param string $roleIds 1,2,3
     * @return bool
     */
    public function validateRoleIds(string $roleIds) : bool
    {
        if (empty($roleIds)) {
            return false;
        }

        // Get each of the individual roleIds into an array
        $roleIdArray = explode(',', $roleIds);

        // Walk through each array item and evaluate if its
        // a valid role id or not.
        $notValid = 0;

        array_walk($roleIdArray, function ($roleId) use (&$notValid) {
            $roleId = trim($roleId);    // Remove any whitespace
            if (!is_numeric($roleId)) {
                $notValid += 1;
            } else {
                $role = Role::find($roleId);
                $notValid += $role ? 0 : 1;
            }
        });

        return $notValid ? false : true;
    }

    /**
     * Validates passed attributes to make sure all the required fields are present
     * and correct for the record type.
     * @param array $attributeArray
     * @param bool $newRecordMode
     * @throws \Exception
     */
    public function validate(array $attributeArray, bool $newRecordMode) : void
    {
        // Validate all inputs are sane
        $userValidator =
            v::attribute('firstName', v::alnum()->noWhitespace()->length(1, 80))
                ->attribute('middleName', v::optional(v::alnum()->noWhitespace()->length(1, 80)))
                ->attribute('lastName', v::alnum()->noWhitespace()->length(1, 80))
                ->attribute('countryId', v::optional(v::intVal()))
                ->attribute('timezoneId', v::optional(v::intVal()))
                ->attribute('email', v::email())
                ->attribute('password', $newRecordMode ?
                    v::stringType()->length(6, 80) :
                    v::optional(v::stringType()->length(6, 80)))
                ->attribute('roleIds', v::optional(v::stringType()->length(1, 255)))
        ;

        $userValidator->assert((object)$attributeArray);
    }

    public function update(
        ServerRequestInterface $request,
        Model $loggedInUser,
        int $updateId,
        array $attributeArray
    ) {
        // Get the model item that the user is trying to update
        $item = $this->getById($loggedInUser->groupId, $updateId);
        if (!$item) {
            throw new ItemDoesNotExistException();
        }

        if ($this->isAdmin($request)) {
            // Ensure roleIds are ok
            if (!$this->validateRoleIds($attributeArray['roleIds'] ?? '')) {
                throw new \Exception('One or more of the provided role ids are invalid');
            }
        }

        parent::doUpdate($loggedInUser, $item, $request->getAttribute('ownRecordsOnly'), $attributeArray);

        // If the user is wanting to update the password, handle that now.
        if ((isset($attributeArray['password'])) && (!empty($attributeArray['password']))) {
            $item->password = $this->hashPassword($attributeArray['password']);
            $item->save();
        }

        // If the logged in user is an administrator, allow them to update roles
        if ($this->isAdmin($request)) {
            DB::transaction(function () use ($updateId, $attributeArray) {
                // Remove any existing roles for this user
                $userRoles = UserRole::where('userId', $updateId)
                    ->get();

                if (count($userRoles) > 0) {
                    /**
                     * @var UserRole $userRole
                     */
                    foreach ($userRoles as $userRole) {
                        $userRole->delete();
                    }
                }

                // Get the new roleIds
                $roleIdArray = explode(',', $attributeArray['roleIds']);

                // Insert new role assignments
                foreach ($roleIdArray as $roleId) {
                    $userRole = new UserRole();

                    $userRole->roleId = $roleId;
                    $userRole->userId = $updateId;

                    if (!$userRole->save()) {
                        throw new \Exception('Unable to save user role');
                    }
                }
            });
        }
    }

    /**
     * Determines whether or not the logged in user is an administrator or not.
     * Note, this method expects that the 'authenticator' middleware will set userRoles into
     * the request object.
     * @param ServerRequestInterface $request
     * @return bool
     * @throws \Exception
     */
    public function isAdmin(ServerRequestInterface $request)
    {
        /**
         * @var array $roleIdArray
         */
        $roleIdArray = $request->getAttribute('userRoles');

        if (!is_array($roleIdArray)) {
            return false;
        }

        $adminRoleId = (int) $this->container->get('settings')['user']['adminRoleId'] ?? 0;

        if (!$adminRoleId) {
            throw new \Exception('adminRoleId not defined in user settings');
        }

        /**
         * @var Role $role
         */
        foreach ($roleIdArray as $role) {
            if ($role->roleId == $adminRoleId) {
                return true;
            }
        }

        return false;
    }
}
