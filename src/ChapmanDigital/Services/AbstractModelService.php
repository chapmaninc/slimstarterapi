<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 02/04/2017
 * Time: 09:02
 */

namespace ChapmanDigital\Services;

use ChapmanDigital\Exceptions\AccessDeniedException;
use ChapmanDigital\Services\Interfaces\ModelService;
use Psr\Http\Message\ServerRequestInterface;
use \Slim\Container;
use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;

abstract class AbstractModelService implements ModelService
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Returns the DI container to any consumer that needs it.
     * @return Container
     */
    public function getContainer() : Container
    {
        return $this->container;
    }

    /**
     * Deletes an existing record from the database.  Note, the child model must implement
     * the getById method for this to work.
     * @param Model $loggedInUser
     * @param int $deleteId
     * @param bool $ownRecordsOnly
     * @throws AccessDeniedException
     */
    public function delete(Model $loggedInUser, int $deleteId, bool $ownRecordsOnly) : void
    {
        // Validate that groupId and deleteId are OK
        $deleteData = [
            'groupId' => $loggedInUser->groupId,
            'deleteId' => $deleteId
        ];

        $validator =
            v::attribute('groupId', v::intVal())
                ->attribute('deleteId', v::intVal());

        $validator->assert((object)$deleteData);

        /**
         * Call the getById method in the child model.  This *must* be implemented.
         * @var Model $item
         */
        $item = $this->getById($loggedInUser->groupId, $deleteId);

        // If the user is only allowed to delete records that they themselves created,
        // and this user was not created by them, throw an access denied exception.
        if (($ownRecordsOnly) && ($item->createdByUserId != $loggedInUser->userId)) {
            throw new AccessDeniedException();
        }

        $item->delete();
    }

    /**
     * If the user is only allowed to affect records that they themselves created,
     * and this item/record was not created by them, throw an access denied exception.
     * @param Model $loggedInUser
     * @param Model $item
     * @param bool $ownRecordsOnly
     * @throws AccessDeniedException
     */
    protected function enforceOwnRecordsOnly(Model $loggedInUser, Model $item, bool $ownRecordsOnly)
    {
        if (($ownRecordsOnly) && ($item->createdByUserId != $loggedInUser->userId)) {
            throw new AccessDeniedException();
        }
    }

    /**
     * Do not provide implementation here.  Implement in child / overriding class.
     * @param array $attributeArray
     * @param bool $newRecordMode
     */
    public function validate(array $attributeArray, bool $newRecordMode)
    {
    }

    /**
     * Do not provide implementation here.  Implement in child / overriding class.
     * @param ServerRequestInterface $request
     * @param Model $loggedInUser
     * @param int $updateId
     * @param array $attributeArray
     */
    public function update(
        ServerRequestInterface $request,
        Model $loggedInUser,
        int $updateId,
        array $attributeArray
    ) {
    }

    /**
     * Validates and then Updates a model $item with the new values contained in $attributeArray,
     * enforcing ownRecordsOnly permission.
     * @param Model $loggedInUser
     * @param Model $item
     * @param bool $ownRecordsOnly
     * @param array $attributeArray
     * @throws \Exception
     */
    protected function doUpdate(Model $loggedInUser, Model $item, bool $ownRecordsOnly, array $attributeArray)
    {
        // Ensure the use has permission to update this record.
        $this->enforceOwnRecordsOnly($loggedInUser, $item, $ownRecordsOnly);

        // Ensure the passed attributes are valid for the record.
        $this->validate($attributeArray, false);

        // Fill the model with new values
        $item->fill($attributeArray);

        // Save the item
        if (!$item->save()) {
            throw new \Exception('Failed to save item');
        }
    }
}