<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 18/03/2017
 * Time: 10:44
 */

namespace ChapmanDigital\Services\Interfaces;

use Psr\Http\Message\ServerRequestInterface;
use \Slim\Container;
use Illuminate\Database\Eloquent\Model;

interface ModelService
{
    public function __construct(Container $container);
    public function getContainer() : Container;
    public function delete(Model $loggedInUser, int $deleteId, bool $ownRecordsOnly);
    public function validate(array $attributeArray, bool $newRecordMode);
    public function update(
        ServerRequestInterface $request,
        Model $loggedInUser,
        int $updateId,
        array $attributeArray
    );
}
