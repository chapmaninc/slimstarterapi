<?php
namespace ChapmanDigital\Models;

class GeoModel
{
    const GEO_IP_SERVICE_URL = 'http://ip-api.com/json/';

    public function getCountryFromIp(string $ip, string $cachePath, string $dummyIP)
    {
        // Define blank result
        $result = [
            "as" => "",
            "city" => "",
            "country" => "",
            "countryCode" => "",
            "isp" => "",
            "lat" => "",
            "lon" => "",
            "org" => "",
            "query" => "",
            "region" => "",
            "regionName" => "",
            "status" => "fail",
            "timezone" => "",
            "zip" => ""
        ];
        
        // If the IP is a local ip address, use a fixed IP for testing.
        if (empty($ip)) {
            return $result;
        } elseif (($ip == "::1") || ($ip == "127.0.0.1") || (strstr($ip, "192.168.1."))) { // developer mode
            $ip = $dummyIP;
        }
        
        // Have we looked up this IP before?  If so, use the cache value.
        $cacheFilePath = $cachePath . "/" . md5($ip);
        if (file_exists($cacheFilePath)) {
            $result = json_decode(file_get_contents($cacheFilePath));
        } else {
            // The result was not in the cache, grab it from the geo service
            $apiURL = self::GEO_IP_SERVICE_URL . $ip;
            $jsonText = trim(@file_get_contents($apiURL));
            
            if (empty($jsonText)) {
                return $result;
            }

            // Store the result in the cache for next time.
            file_put_contents($cacheFilePath, $jsonText);
            
            $result = json_decode($jsonText);
        }

        return $result;
    }
}