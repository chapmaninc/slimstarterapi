<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class RolePermission extends Model
{
    protected $table = 'rolepermission';
    public $primaryKey = 'rolePermissionId';
    public $timestamps = false;

    const OPERATION_TYPE_CREATE = 1;
    const OPERATION_TYPE_READ = 2;
    const OPERATION_TYPE_UPDATE = 3;
    const OPERATION_TYPE_DELETE = 3;

    const PERMISSION_NO_PERMISSION = 0;
    const PERMISSION_OWN_RECORDS_ONLY = 1;
    const PERMISSION_ALL_GROUP_RECORDS = 2;

    public static function getRolePermissionsForUserRolesAndPermissionType(
        array $roles,
        int $permissionTypeId
    ) {
        if ((count($roles) == 0) || ($permissionTypeId < 1)) {
            return null;
        }

        $roleIdArray = Role::getRoleIdsFromRoleObjectArray($roles);
        if (count($roleIdArray) == 0) {
            return null;
        }

        $results = RolePermission::where('permissionTypeId', $permissionTypeId)
            ->whereIn('roleId', $roleIdArray)->get();

        return $results;
    }

    public static function hasPermission(Collection $rolePermissions, int $operationType) : int
    {
        if (count($rolePermissions) == 0) {
            return false;
        }

        $result = self::PERMISSION_NO_PERMISSION;

        /**
         * @var RolePermission $rolePermission
         */
        foreach ($rolePermissions as $rolePermission) {
            switch ($operationType) {
                case self::OPERATION_TYPE_CREATE:
                    if ($rolePermission->canCreate) {
                        if ($rolePermission->ownRecordsOnly == 0) {
                            return self::PERMISSION_ALL_GROUP_RECORDS;
                        } else {
                            $result = self::PERMISSION_OWN_RECORDS_ONLY;
                        }
                    }

                    break;

                case self::OPERATION_TYPE_READ:
                    if ($rolePermission->canRead) {
                        if ($rolePermission->ownRecordsOnly == 0) {
                            return self::PERMISSION_ALL_GROUP_RECORDS;
                        } else {
                            $result = self::PERMISSION_OWN_RECORDS_ONLY;
                        }
                    }

                    break;

                case self::OPERATION_TYPE_UPDATE:
                    if ($rolePermission->canUpdate) {
                        if ($rolePermission->ownRecordsOnly == 0) {
                            return self::PERMISSION_ALL_GROUP_RECORDS;
                        } else {
                            $result = self::PERMISSION_OWN_RECORDS_ONLY;
                        }
                    }

                    break;

                case self::OPERATION_TYPE_DELETE:
                    if ($rolePermission->canDelete) {
                        if ($rolePermission->ownRecordsOnly == 0) {
                            return self::PERMISSION_ALL_GROUP_RECORDS;
                        } else {
                            $result = self::PERMISSION_OWN_RECORDS_ONLY;
                        }
                    }

                    break;
            }
        }

        return $result;
    }
}
