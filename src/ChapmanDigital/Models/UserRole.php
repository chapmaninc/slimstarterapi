<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'userrole';
    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'modifiedDate';
    public $primaryKey = 'userRoleId';

    /**
     * Get the role record associated with the UserRole.
     */
    public function role()
    {
        return $this->hasOne('\ChapmanDigital\Models\Role', 'roleId', 'roleId');
    }
}