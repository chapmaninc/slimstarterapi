<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'token';
    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'modifiedDate';
    public $primaryKey = 'tokenCode';
    public $incrementing = false;

    // Define fields that can be mass filled.
    protected $fillable = ['userId', 'ipAddress'];
}
