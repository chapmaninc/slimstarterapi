<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionType extends Model
{
    protected $table = 'permissiontype';
    public $timestamps = false;
    public $primaryKey = 'permissionTypeId';
}