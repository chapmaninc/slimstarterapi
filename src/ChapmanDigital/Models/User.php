<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 22/02/2017
 * Time: 20:47
 */

namespace ChapmanDigital\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $table = 'user';
    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'modifiedDate';
    const DELETED_AT = 'deletedDate';
    public $primaryKey = 'userId';

    // Define fields that can be mass filled.
    protected $fillable = ['firstName', 'lastName', 'middleName', 'countryId', 'timezoneId', 'email'];

    /**
     * Get the UserRole records for the user.
     */
    public function userRoles()
    {
        return $this->hasMany('ChapmanDigital\Models\UserRole', 'userId', 'userId');
    }
}