<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 22/02/2017
 * Time: 20:47
 */

namespace ChapmanDigital\Models;

use ChapmanDigital\Exceptions\EmailAlreadyExistsException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;

class Group extends Model
{
    protected $table = 'group';
    const CREATED_AT = 'createdDate';
    const UPDATED_AT = 'modifiedDate';
    public $primaryKey = 'groupId';

    // Define fields that can be mass filled.
    protected $fillable = ['enabled', 'name', 'countryId', 'timezoneId'];
}