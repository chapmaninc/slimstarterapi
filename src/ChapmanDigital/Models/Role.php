<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    public $primaryKey = 'roleId';
    public $timestamps = false;

    /**
     * Builds an array of roleIds from an array of user role objects
     * @param array $roles An array of user role objects
     * @return array
     */
    public static function getRoleIdsFromRoleObjectArray(array $roles) : array
    {
        $roleIds = [];

        /**
         * @var Role $role
         */
        $roleIds = array_map(function ($role) {
            return $role->roleId;
        }, $roles);

        return $roleIds;
    }
}
