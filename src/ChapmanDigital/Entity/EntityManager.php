<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 14/03/2017
 * Time: 21:13
 */

namespace ChapmanDigital\Entity;

use ChapmanDigital\Entity\Interfaces\EntityInterface;

class EntityManager
{
    public function setEntity(EntityInterface &$entity, Model $model)
    {
        $fields = $entity->getFields();

        foreach ($fields as $fieldName) {
            $entity->$fieldName = $model->$fieldName;
        }
    }
}
