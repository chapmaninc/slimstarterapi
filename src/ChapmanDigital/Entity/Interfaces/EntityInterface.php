<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 14/03/2017
 * Time: 21:15
 */

namespace ChapmanDigital\Entity\Interfaces;

interface EntityInterface
{
    public function getFields() : array;
}
