<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 14/03/2017
 * Time: 21:05
 */

namespace ChapmanDigital\Entity\Entity;

use ChapmanDigital\Entity\Interfaces\EntityInterface;

class TokenEntity implements EntityInterface
{
    public $tokenCode;
    public $createdDate;
    public $modifiedDate;
    public $expiryDate;
    public $userId;
    public $ipAddress;

    public function getFields() : array
    {
        return [
            'tokenCode',
            'createdDate',
            'modifiedDate',
            'expiryDate',
            'userId',
            'ipAddress'
        ];
    }
}