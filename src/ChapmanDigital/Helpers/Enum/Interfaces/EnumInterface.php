<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 25/03/2017
 * Time: 10:03
 */

namespace ChapmanDigital\Helpers\Enum\Interfaces;

interface EnumInterface
{
    public function getCaption(int $option) : string;
    public function getOptions() : array;
}
