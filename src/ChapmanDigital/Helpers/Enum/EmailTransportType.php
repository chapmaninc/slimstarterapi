<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 25/03/2017
 * Time: 09:21
 */

namespace ChapmanDigital\Helpers\Enum;

class EmailTransportType extends AbstractEnum
{
    const SMTP = 0;
    const SEND_MAIL = 1;
    const PHP_NATIVE = 2;

    public function getCaption(int $option) : string
    {
        $caption = '';

        switch ($option) {
            case self::SMTP:
                $caption = 'SMTP';
                break;

            case self::SEND_MAIL:
                $caption = 'SEND MAIL';
                break;

            case self::PHP_NATIVE:
                $caption = 'PHP MAIL';
                break;

            default:
                throw new \Exception("EmailTransportType::getCaption - Unresolved option: $option");
                break;
        }


        return $caption;
    }

    /**
     * Returns all of the option values as an associative array
     * @return array
     */
    public function getOptions() : array
    {
        return [
            self::SMTP => $this->getCaption(self::SMTP),
            self::SEND_MAIL => $this->getCaption(self::SEND_MAIL),
            self::PHP_NATIVE => $this->getCaption(self::PHP_NATIVE)
        ];
    }
}
