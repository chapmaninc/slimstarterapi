<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 25/03/2017
 * Time: 10:02
 */

namespace ChapmanDigital\Helpers\Enum;

use ChapmanDigital\Helpers\Enum\Interfaces\EnumInterface;

abstract class AbstractEnum implements EnumInterface
{
    /**
     * Returns the key for a particular value, and the default value if the key does not exist.
     * @param string $caption
     * @param int $defaultKey
     * @return mixed
     * @throws \Exception
     */
    public function getKeyFromCaption(string $caption, int $defaultKey)
    {
        $options = $this->getOptions();

        $key = array_search($caption, $options);

        if ($key) {
            return $key;
        } else {
            if (!isset($options[$defaultKey])) {
                throw new \Exception('AbstractEnum::getKeyFromCaption Invalid key: ' . $defaultKey);
            }

            return $options[$defaultKey];
        }
    }
}