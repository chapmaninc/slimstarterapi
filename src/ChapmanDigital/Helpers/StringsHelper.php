<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 14/03/2017
 * Time: 20:11
 */

namespace ChapmanDigital\Helpers;

/**
 * Class Strings
 * @package ChapmanDigital\Helpers
 */
class StringsHelper
{
    /**
     * Creates a random string of the specified length.
     * @param int $len
     * @return string
     */
    public function createRandomString(int $len):string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}