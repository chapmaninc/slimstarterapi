<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 25/03/2017
 * Time: 08:50
 */

namespace ChapmanDigital\Helpers;

use Slim\Container;
use ChapmanDigital\Helpers\Enum\EmailTransportType;
use Swift_SmtpTransport;
use Swift_Message;
use Swift_Mailer;
use Swift_Attachment;
use Swift_MailTransport;
use Swift_SendmailTransport;
use Twig_Loader_Filesystem;
use Twig_Environment;

class EmailHelper
{
    private $container; // DI Container

    /**
     * @var array $emailSettings
     */
    private $emailSettings;
    private $emailFromName;
    private $emailFrom;
    private $emailReplyTo;
    private $emailTemplateDirectory;

    private $transportMethod;
    private $transportHost;
    private $attachments;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->attachments = [];
        $this->emailSettings = $this->container->get('settings')['email'];

        // Replace the install path
        /**
         * @var EnvironmentHelper $environmentHelper
         */
        $environmentHelper = $this->container->get('EnvironmentHelper');

        if (!is_array($this->emailSettings)) {
            throw new \Exception('Invalid configuration - email settings not found');
        }

        $this->emailFrom = $this->emailSettings['from'] ?? 'admin@mywebsite.com';
        $this->emailFromName = $this->emailSettings['fromName'] ?? 'John Doe';
        $this->emailReplyTo = $this->emailSettings['replyTo'] ?? $this->emailFrom;
        $this->emailTemplateDirectory = $this->emailSettings['templateDir'] ?? '';

        // Get the transport method from the settings, and default to SMTP if none defined.
        $this->transportMethod = (new EmailTransportType())->getKeyFromCaption(
            $this->emailSettings['transportMethod'] ?? 'SMTP',
            EmailTransportType::SMTP
        );

        $this->transportHost = $this->emailSettings['serverHost'] ?? 'localhost';

        $environmentHelper->replaceInstallPath($this->emailTemplateDirectory);

        if ((empty($this->emailTemplateDirectory)) || (!is_dir($this->emailTemplateDirectory))) {
            throw new \Exception('Email template directory defined in settings does not exist: ' .
            $this->emailTemplateDirectory);
        }

        if (!$this->isValidEmail($this->emailFrom)) {
            throw new \Exception('Invalid from email address: ' . $this->emailFrom);
        }

        if (!$this->isValidEmail($this->emailReplyTo)) {
            throw new \Exception('Invalid reply-to email address: ' . $this->emailReplyTo);
        }
    }

    public function setFromName(string $fromName)
    {
        $this->emailFromName = $fromName;
    }

    public function setFrom(string $emailFrom)
    {
        if (!$this->isValidEmail($emailFrom)) {
            throw new \Exception('Invalid email from address: ' . $emailFrom);
        }

        $this->emailFrom = $emailFrom;
    }

    public function setReplyTo(string $replyTo)
    {
        if (!$this->isValidEmail($replyTo)) {
            throw new \Exception('Invalid reply-to email address: ' . $replyTo);
        }

        $this->emailReplyTo = $replyTo;
    }

    /**
     * Validates whether an email address is valid or not.
     * @param string $email
     * @return bool
     */
    public function isValidEmail(string $email) : bool
    {
        if (empty($email)) {
            return false;
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return false;
        }

        return true;
    }

    /**
     * Appends a file as an attachment to the email.
     * @param string $attachmentPath
     * @throws \Exception
     */
    public function addAttachment(string $attachmentPath)
    {
        if (!file_exists($attachmentPath)) {
            throw new \Exception('Attachment path does not exist: ' . $attachmentPath);
        }

        $this->attachments[] = $attachmentPath;
    }

    public function sendEmail(
        string $to,
        string $toName,
        string $subject,
        string $bodyPlainText,
        string $bodyHtml
    ) {
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->emailFrom, $this->emailFromName)
            ->setTo($to, $toName)
            ->setBody($bodyPlainText);

        // Add the HTML version if present
        if (!empty($bodyHtml)) {
            $message->addPart($bodyHtml, 'text/html');
        }

        // Add attachments to the message
        if (count($this->attachments) > 0) {
            foreach ($this->attachments as $attachmentPath) {
                $message->attach(Swift_Attachment::fromPath($attachmentPath));
            }
        }

        $headers = $message->getHeaders();
        $headers->addTextHeader('Reply-To', $this->emailReplyTo);

        switch ($this->transportMethod) {
            case EmailTransportType::SMTP:
                $smtpPort = $this->emailSettings['smtpServerPort'] ?? 25;
                $smtpUsername = $this->emailSettings['smtpServerUsername'] ?? '';
                $smtpPassword = $this->emailSettings['smtpServerPassword'] ?? '';
                $smtpEncryptionMethod = $this->emailSettings['smtpServerEncryptionMethod'] ?? '';

                $transport = Swift_SmtpTransport::newInstance($this->transportHost, $smtpPort);

                if ((!empty($smtpUsername)) && (!empty($smtpUsername))) {
                    $transport->setUsername($smtpUsername)
                        ->setPassword($smtpPassword);
                }

                if (!empty($smtpEncryptionMethod)) {
                    $transport->setEncryption($smtpEncryptionMethod);
                }
                break;

            case EmailTransportType::SEND_MAIL:
                $sendmailCommand = $this->emailSettings['sendmailCommand'] ?? '/usr/sbin/sendmail -bs';
                $transport = Swift_SendmailTransport::newInstance($sendmailCommand);
                break;

            case EmailTransportType::PHP_NATIVE:
                $transport = Swift_MailTransport::newInstance();
                break;

            default:
                throw new \Exception('Unhandled Email Transport Method: ' . $this->transportMethod);
                break;
        }

        $mailer = Swift_Mailer::newInstance($transport);
        return $mailer->send($message);
    }

    public function sendPlainTextEmail(string $to, string $toName, string $subject, string $body)
    {
        return $this->sendEmail($to, $toName, $subject, $body, '');
    }

    public function sendHtmlEmail(
        string $to,
        string $toName,
        string $subject,
        string $templateName,
        array $templateData = []
    ) {
        /**
         * @var EnvironmentHelper $environmentHelper
         */
        $environmentHelper = $this->container->get('EnvironmentHelper');

        $templatePath = $environmentHelper->getInstallPath() . 'templates/';
        $cachePath = $environmentHelper->getInstallPath() . 'cache/';

        // Load the HTML template and substitute variables.
        $loader = new Twig_Loader_Filesystem($templatePath . 'email/');
        $twig = new Twig_Environment($loader, array(
            'cache' => $cachePath,
        ));

        $html = $twig->render($templateName . '.html', $templateData);

        // Load the new plain text version too if it exists
        $plainTextBody = '';
        $txtDirPath = $templatePath . 'email/plaintext/';
        $txtTemplatePath = $txtDirPath . $templateName . '.txt';
        if (file_exists($txtTemplatePath)) {
            $loader = new Twig_Loader_Filesystem($txtDirPath);
            $twig = new Twig_Environment($loader, array(
                'cache' => $cachePath,
            ));

            $plainTextBody = $twig->render($templateName . '.txt', $templateData);
        }

        return $this->sendEmail($to, $toName, $subject, $plainTextBody, $html);
    }
}
