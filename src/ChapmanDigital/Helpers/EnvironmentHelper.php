<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 09/03/2017
 * Time: 21:15
 */

namespace ChapmanDigital\Helpers;

use Slim\Container;
use Psr\Http\Message\ServerRequestInterface;

class EnvironmentHelper
{
    private $container; // DI Container

    /**
 * @var array $environment Environment settings
 */
    private $environment;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->environment = $this->container->get('settings')['environment'];

        if (!is_array($this->environment)) {
            throw new \Exception('Invalid configuration - environment settings not found');
        }
    }

    /**
     * Gets the baseURL for the API, e.g http://localhost/
     * @return string
     * @throws \Exception
     */
    public function getBaseURL():string
    {
        $baseURL = $this->environment['baseURL'] ?? '';

        if (empty($baseURL)) {
            throw new \Exception('Invalid configuration - baseURL not found in settings');
        }

        return $baseURL;
    }

    /**
     * Returns the slug for the current request, e.g. "authenticated/user"
     * @param ServerRequestInterface $request
     * @return string
     */
    public function getUriSlug(ServerRequestInterface $request):string
    {
        $uri = $request->getUri();

        $uri = $this->stripIdFromUri($uri);

        $baseURL = $this->getBaseURL();

        if (substr($baseURL, -1) != '/') {
            $baseURL .= '/';
        }

        $slug = '/' . str_replace($baseURL, '', $uri);

        return $slug;
    }

    /**
     * If an id number is present at the end of a URL, e.g. /my/uri/25, the number will be
     * removed, and the uri without the number returned.  Otherwise the uri untouched is returned.
     * @param string $uri
     * @return string
     */
    private function stripIdFromUri(string $uri)
    {
        $lastSlashPos = strrpos($uri, '/');
        if ($lastSlashPos === false) {
            return $uri;
        }

        $strAfterSlash = substr($uri, $lastSlashPos + 1);

        if (is_numeric($strAfterSlash)) {
            return substr($uri, 0, $lastSlashPos);
        }

        return $uri;
    }

    /**
     * Gets the installPath where the code is running
     * @return string
     * @throws \Exception
     */
    public function getInstallPath():string
    {
        $installPath = $this->environment['installPath'] ?? '';

        if (empty($installPath)) {
            throw new \Exception('Invalid configuration - installPath not found in settings');
        }

        if (substr($installPath, -1) != '/') {
            $installPath .= '/';
        }

        return $installPath;
    }

    /**
     * Gets the dummy IP to use when code is running on localhost
     * @return string
     * @throws \Exception
     */
    public function getDummyIP():string
    {
        $dummyIP = $this->environment['dummyIP'] ?? '';

        if (empty($dummyIP)) {
            throw new \Exception('Invalid configuration - dummyIP not found in settings');
        }

        return $dummyIP;
    }

    /**
     * Searches the target string for the %INSTALL_PATH% constant
     * and replaces it with the real one.
     * @param string $target Passed by reference
     */
    public function replaceInstallPath(string &$target)
    {
        if (empty($target)) {
            return;
        }

        $target = str_replace(
            '%INSTALL_PATH%',
            $this->getInstallPath(),
            $target
        );
    }
}
