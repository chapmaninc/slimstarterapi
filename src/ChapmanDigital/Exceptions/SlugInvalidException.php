<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Exceptions;

use \Exception;

/**
 * Class SlugInvalidException
 * @package ChapmanDigital\Exceptions
 */
class SlugInvalidException extends Exception
{
    public function __construct(string $slug)
    {
        parent::__construct(
            'Invalid Slug Exception.  The slug "' . $slug . '" has no permissions defined.',
            ERRORCODE_SLUG_INVALID,
            null
        );
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}