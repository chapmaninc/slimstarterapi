<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Exceptions;

use \Exception;

/**
 * Class TokenInvalidException
 * @package ChapmanDigital\Exceptions
 */
class UrlInvalidException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'Invalid URL Exception',
            ERRORCODE_URL_INVALID,
            null
        );
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}