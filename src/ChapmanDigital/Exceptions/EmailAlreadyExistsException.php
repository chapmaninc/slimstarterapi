<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Exceptions;

use \Exception;

/**
 * Class TokenExpiredException
 * @package ChapmanDigital\Exceptions
 */
class EmailAlreadyExistsException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'User Already Exists Exception',
            ERRORCODE_EMAIL_ALREADY_EXISTS,
            null
        );
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}