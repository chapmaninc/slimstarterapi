<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Exceptions;

use \Exception;

/**
 * Class AccessDeniedException
 * @package ChapmanDigital\Exceptions
 */
class AccessDeniedException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'Access Denied Exception',
            ERRORCODE_ACCESS_DENIED,
            null
        );
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}