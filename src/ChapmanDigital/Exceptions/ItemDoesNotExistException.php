<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Exceptions;

use \Exception;

/**
 * Class ItemDoesNotExistException
 * @package ChapmanDigital\Exceptions
 */
class ItemDoesNotExistException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'Item Does Not Exist Exception',
            ERRORCODE_ITEM_DOES_NOT_EXIST,
            null
        );
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
