<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace ChapmanDigital\Exceptions;

use \Exception;

/**
 * Class TokenExpiredException
 * @package ChapmanDigital\Exceptions
 */
class TokenExpiredException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'Token Expired Exception',
            5,
            null
        );
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}