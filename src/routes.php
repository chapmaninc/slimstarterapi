<?php
// Routes

// Setup IPAddress detection middleware
$checkProxyHeaders = true;
$trustedProxies = []; // ['10.0.0.1', '10.0.0.2']
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));

// Public routes
$app->get('/geo/timezone', GeographyController::class . ':getTimezones');
$app->get('/geo/timezone/{countryCode}', GeographyController::class . ':getTimezones');
$app->get('/geo/country', GeographyController::class . ':getUserCountry');
$app->get('/geo/user-timezones', GeographyController::class . ':getUserTimezones');
$app->post('/group/register', GroupController::class . ':register');
$app->post('/user/login', UserController::class . ':login');

// Authenticated routes
$app->group('/authenticated', function () use ($app) {
    $app->put('/group/{groupId:[0-9]+}', GroupController::class . ':updateGroup');
    $app->post('/user', UserController::class . ':addUser');
    $app->put('/user/{userId:[0-9]+}', UserController::class . ':updateUser');
    $app->delete('/user/{userId:[0-9]+}', UserController::class . ':deleteUser');
})->add(Authenticator::class);
