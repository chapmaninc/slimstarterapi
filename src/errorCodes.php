<?php
define('ERRORCODE_GENERAL_EXCEPTION', 0);
define('ERRORCODE_COULD_NOT_RESOLVE_IP', 1);
define('ERRORCODE_VALIDATION_ERROR', 2);
define('ERRORCODE_EMAIL_ALREADY_EXISTS', 3);
define('ERRORCODE_LOGIN_FAILURE', 4);
define('ERRORCODE_TOKEN_EXPIRED', 5);
define('ERRORCODE_TOKEN_INVALID', 6);
define('ERRORCODE_URL_INVALID', 7);
define('ERRORCODE_SLUG_INVALID', 8);
define('ERRORCODE_ITEM_DOES_NOT_EXIST', 9);
define('ERRORCODE_ACCESS_DENIED', 10);

return [
    ERRORCODE_GENERAL_EXCEPTION => [
        'message' => 'An application error occurred.',
        'httpStatusCode' => 500,
    ],
    ERRORCODE_COULD_NOT_RESOLVE_IP => [
        'message' => 'Could not resolve IP address',
        'httpStatusCode' => 500,
    ],
    ERRORCODE_VALIDATION_ERROR => [
        'message' => 'Validation Error',
        'httpStatusCode' => 400,
    ],
    ERRORCODE_EMAIL_ALREADY_EXISTS => [
        'message' => 'A user with this email address already exists',
        'httpStatusCode' => 400,
    ],
    ERRORCODE_LOGIN_FAILURE => [
        'message' => 'Sorry, your login failed.  Please check your email address and password and try again.',
        'httpStatusCode' => 400,
    ],
    ERRORCODE_TOKEN_EXPIRED => [
        'message' => 'Sorry, your token has expired.  Please generate a new token.',
        'httpStatusCode' => 403,
    ],
    ERRORCODE_TOKEN_INVALID => [
        'message' => 'Sorry, your token is invalid.  Please generate a new token.',
        'httpStatusCode' => 403,
    ],
    ERRORCODE_URL_INVALID => [
        'message' => 'Sorry, the URL being loaded is invalid.  Please try again.',
        'httpStatusCode' => 500,
    ],
    ERRORCODE_SLUG_INVALID => [
        'message' => 'Sorry, the slug being processed has no permission records.',
        'httpStatusCode' => 400,
    ],
    ERRORCODE_ITEM_DOES_NOT_EXIST => [
        'message' => 'Sorry, the item you are trying to modify does not exist.',
        'httpStatusCode' => 404,
    ]
];
