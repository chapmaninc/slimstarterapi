# Welcome to SlimStarter

If you're building a new PHP application that needs to provide a Restful API then SlimStarter is 
for you!

SlimStarter builds upon the Slim V3 PHP Framework to provide a set of services that most 
restful API projects will need.  These services include:

* User management (registration, login, password reset);
* Session authentication (using tokens);
* User roles and route permissions (i.e. only users that belong to certain roles can perform certain actions);

The project goal is to bootstrap your project very quickly and remove the need to reinvent the
wheel.

## Features Coming SOon

* A demonstration admin console for managing users and route permissions;
* Email sending and a default email template for notifications

## Dependencies

* PHP 7+
* Composer
* MySQL

## Status

The project is current in an alpha status, but if you're wanting to take a look, you can clone this repo and set it up.

You will need to:

* Create a MySQL database and restore db.sql;
* Run composer update to install the vendor libraries;
* Copy src/settings.sample.php to src/settings.php and adjust the db connection settings and also the environment intsallation path;

